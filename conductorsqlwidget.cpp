#include "conductorsqlwidget.h"

#include "colordelegate.h"
#include "sqlcoloreditwidget.h"
#include "sqlcheckboxeditwidget.h"
#include "sqltexteditwidget.h"

#include "extendedtableview.h"

QString appearanceColorColumn = "AppearanceColor";
QString appearanceColor2Column = "AppearanceSecondaryColor";

namespace  {
    // index 0 is by default a dummy, but can be overwritten
    int colorEditWidgetIndex = 1;
    int checkBoxEditWidgetIndex = 2;
    int textEditWidgetIndex = 3;
}

ConductorSqlWidget::ConductorSqlWidget(QWidget *parent): SqlWidget(parent)
{
    setViewDelegate(new ColorDelegate(view()));

    m_color = new SqlColorEditWidget(this);
    addEditWidget(m_color, colorEditWidgetIndex);
    addEditWidget(new SqlCheckBoxEditWidget(this), checkBoxEditWidgetIndex);
    addEditWidget(new SqlTextEditWidget(this), textEditWidgetIndex);
}

int ConductorSqlWidget::changeEditWidget(QModelIndexList& itms) {

    int column = itms[0].column();
    QString header = headerData(column);
    if (header == appearanceColorColumn || header == appearanceColor2Column)
        return colorEditWidgetIndex;
    else if (header == "Type")
        return checkBoxEditWidgetIndex;

    QString t = type(column);
    if (t == "TEXT") {
        return textEditWidgetIndex;
    } else if (t == "REAL") {
        // TODO implement
    }
    return 0;
}
