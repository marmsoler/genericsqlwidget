#ifndef SQLWIDGETPRIVATE_H
#define SQLWIDGETPRIVATE_H

#include <QtSql/QSqlDatabase>

class SqlTableModel;

class SqlWidgetPrivate
{
public:
    SqlWidgetPrivate();
    ~SqlWidgetPrivate();

    // name of the loaded table
    QString table;
    QSqlDatabase db;
    SqlTableModel *model{nullptr};
};

#endif // SQLWIDGETPRIVATE_H
