#ifndef SQLCHECKBOXEDITWIDGET_H
#define SQLCHECKBOXEDITWIDGET_H

#include <sqleditwidget.h>

class QCheckBox;

class SqlCheckBoxEditWidget : public SqlEditWidget
{
public:
    SqlCheckBoxEditWidget(QWidget* parent = nullptr);
private:
    void setWidgetValue(QVariant& value) override;
private:
    QCheckBox* m_check;
};

#endif // SQLCHECKBOXEDITWIDGET_H
