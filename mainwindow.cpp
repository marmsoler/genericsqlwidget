#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QDir>

#include "conductorsqlwidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ConductorSqlWidget* widget = new ConductorSqlWidget(this);
    QDir dir("./../genericsqlwidget/Testdatabases/TestDatabase.sqlite3");
    widget->loadTable(dir.absolutePath(), "Conductors");

    setCentralWidget(widget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

