#include "sqlcoloreditwidget.h"

#include <QLineEdit>
#include <QHBoxLayout>

SqlColorEditWidget::SqlColorEditWidget(QWidget *parent): SqlEditWidget(parent)
{
    m_color = new QLineEdit(this);
    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addWidget(m_color);
    setLayout(hbox);

    connect(m_color, &QLineEdit::textChanged, this, [=] (const QString& value) {
        QVariant temp = value;
        valueChanged(temp);
    });
}

void SqlColorEditWidget::setWidgetValue(QVariant& value) {

    m_color->setText(value.toString());
    return;
}
