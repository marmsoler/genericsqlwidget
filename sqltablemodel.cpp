#include "sqltablemodel.h"

#include "QSqlQuery"

SqlTableModel::SqlTableModel(QObject *parent, QSqlDatabase db): QSqlTableModel(parent, db)
{

}

void SqlTableModel::setQuery(QSqlQuery &query) {
    QSqlTableModel::setQuery(query);
}

int SqlTableModel::IDColumnIndex() const {
    return m_idColumnIndex;
}

int SqlTableModel::NameColumnIndex() const {
    return m_NameColumnIndex;
}

QString SqlTableModel::DefaultName() const {
    return m_DefaultName;
}

void SqlTableModel::setIDColumnIndex(int index) {
    m_idColumnIndex = index;
}

void SqlTableModel::setNameColumnIndex(int index) {
    m_NameColumnIndex = index;
}

void SqlTableModel::setDefaultName(const QString& name) {
    m_DefaultName = name;
}

int SqlTableModel::getID(int row) {
    return data(index(row, m_idColumnIndex)).toInt();
}

void SqlTableModel::setDefaultName(int row) {
    assert(columnCount() > 1);
    setData(index(row, m_NameColumnIndex), m_DefaultName);
}

void SqlTableModel::reset() {
    beginResetModel();
    // TODO: implement
    endResetModel();
}

void SqlTableModel::clearCache() {
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    endRemoveRows();
}

QString SqlTableModel::selectStatement() const {
    QString string = QSqlTableModel::selectStatement();
    return string;
}

void SqlTableModel::update() {
    setTable(tableName()); // fetches field information
    select(); // populate model
    // selectStatement(); // just for checking the statement
}

void SqlTableModel::parseFilterString(int column, const QString &value) {
    // copied from sqlitebrowser, check for License issues!
    // Check for any special comparison operators at the beginning of the value string. If there are none default to LIKE.
    QString op = "LIKE";
    QString val, val2;
    QString escape;
    bool numeric = false, ok = false;

    // range/BETWEEN operator
    if (value.contains("~")) {
        int sepIdx = value.indexOf('~');
        val  = value.mid(0, sepIdx);
        val2 = value.mid(sepIdx+1);
        val.toFloat(&ok);
        if (ok) {
            val2.toFloat(&ok);
            ok = ok && (val.toFloat() < val2.toFloat());
        }
    }
    if (ok) {
        op = "BETWEEN";
        numeric = true;
    } else {
        val.clear();
        val2.clear();
        if(value.left(2) == ">=" || value.left(2) == "<=" || value.left(2) == "<>")
        {
            // Check if we're filtering for '<> NULL'. In this case we need a special comparison operator.
            if(value.left(2) == "<>" && value.mid(2) == "NULL")
            {
                // We are filtering for '<>NULL'. Override the comparison operator to search for NULL values in this column. Also treat search value (NULL) as number,
                // in order to avoid putting quotes around it.
                op = "IS NOT";
                numeric = true;
                val = "NULL";
            } else if(value.left(2) == "<>" && value.mid(2) == "''") {
                // We are filtering for "<>''", i.e. for everything which is not an empty string
                op = "<>";
                numeric = true;
                val = "''";
            } else {
                value.mid(2).toFloat(&numeric);
                op = value.left(2);
                val = value.mid(2);
            }
        } else if(value.left(1) == ">" || value.left(1) == "<") {
            value.mid(1).toFloat(&numeric);
            op = value.left(1);
            val = value.mid(1);
        } else if(value.left(1) == "=") {
            val = value.mid(1);

            // Check if value to compare with is 'NULL'
            if(val != "NULL")
            {
                // It's not, so just compare normally to the value, whatever it is.
                op = "=";
            } else {
                // It is NULL. Override the comparison operator to search for NULL values in this column. Also treat search value (NULL) as number,
                // in order to avoid putting quotes around it.
                op = "IS";
                numeric = true;
            }
        } else {
            // Keep the default LIKE operator

            // Not needed
            // Set the escape character if one has been specified in the settings dialog
//            QString escape_character = Settings::getValue("databrowser", "filter_escape").toString();
//            if(escape_character == "'") escape_character = "''";
//            if(escape_character.length())
//                escape = QString("ESCAPE '%1'").arg(escape_character);

            // Add % wildcards at the start and at the beginning of the filter query, but only if there weren't set any
            // wildcards manually. The idea is to assume that a user who's just typing characters expects the wildcards to
            // be added but a user who adds them herself knows what she's doing and doesn't want us to mess up her query.
            if(!value.contains("%"))
            {
                val = value;
                val.prepend('%');
                val.append('%');
            }
        }
    }
    if(val.isEmpty())
        val = value;

    // If the value was set to an empty string remove any filter for this column. Otherwise insert a new filter rule or replace the old one if there is already one
    if(val == "" || val == "%" || val == "%%")
        m_mWhere.remove(column);
    else {
        // Quote and escape value, but only if it's not numeric and not the empty string sequence
        if(!numeric && val != "''")
            val = QString("'%1'").arg(val.replace("'", "''"));

        QString whereClause(op + " " + QString(val.toUtf8()));
        if (!val2.isEmpty())
            whereClause += " AND " + QString(val2.toUtf8());
        whereClause += " " + escape;
        m_mWhere.insert(column, whereClause);
    }
}

void SqlTableModel::updateFilter(int column, const QString& value, bool applyQuery) {
    // copied from sqlitebrowser, check for license issues!!!

    parseFilterString(column, value);

    // Build the new query
    if (applyQuery) {
        setFilter(where());
    }
}

QString SqlTableModel::where() {
    QString where;

    if(m_mWhere.size())
    {
        //where = "WHERE ";

        for(QMap<int, QString>::const_iterator i=m_mWhere.constBegin();i!=m_mWhere.constEnd();++i)
        {

            QString columnId = headerData(i.key(), Qt::Orientation::Horizontal).toString();
            // ???
//            if(m_vDisplayFormat.size() && m_vDisplayFormat.at(i.key()-1) != columnId)
//                columnId = m_vDisplayFormat.at(i.key()-1);
            where.append(QString("%1 %2 AND ").arg(columnId).arg(i.value()));
        }

        // Remove last 'AND '
        where.chop(4);
    }
    return where;
}

QString SqlTableModel::customQuery(bool withRowid)
{
    QString selector = "*";

    // Note: Building the SQL string is intentionally split into several parts here instead of arg()'ing it all together as one.
    // The reason is that we're adding '%' characters automatically around search terms (and even if we didn't the user could add
    // them manually) which means that e.g. searching for '1' results in another '%1' in the string which then totally confuses
    // the QString::arg() function, resulting in an invalid SQL.
    return QString("SELECT %1 FROM %2 ")
            .arg(selector)
            .arg(tableName())
            + where();
}

void SqlTableModel::setQuery(const QString& sQuery, bool dontClearHeaders) {
    // clear
    if(!dontClearHeaders)
        reset();
    else
        clearCache();

    if(!database().isOpen())
        return;

    QString query_ = sQuery.trimmed();
    removeCommentsFromQuery(query_);

    QSqlQuery query("", database());
    bool success = query.prepare(query_);
    success = query.exec();

    setTable(tableName());

    emit layoutChanged();
}

void SqlTableModel::removeCommentsFromQuery(QString& query)
{
    int oldSize = query.size();

    // first remove block comments
    {
        QRegExp rxSQL("^((?:(?:[^'/]|/(?![*]))*|'[^']*')*)(/[*](?:[^*]|[*](?!/))*[*]/)(.*)$");	// set up regex to find block comment
        QString result;

        while(query.size() != 0)
        {
            int pos = rxSQL.indexIn(query);
            if(pos > -1)
            {
                result += rxSQL.cap(1) + " ";
                query = rxSQL.cap(3);
            } else {
                result += query;
                query = "";
            }
        }
        query = result;
    }

    // deal with end-of-line comments
    {
        /* The regular expression for removing end of line comments works like this:
         * ^((?:(?:[^'-]|-(?!-))*|(?:'[^']*'))*)(--.*)$
         * ^                                          $ # anchor beginning and end of string so we use it all
         *  (                                  )(    )  # two separate capture groups for code and comment
         *                                       --.*   # comment starts with -- and consumes everything afterwards
         *   (?:                 |           )*         # code is none or many strings alternating with non-strings
         *                        (?:'[^']*')           # a string is a quote, followed by none or more non-quotes, followed by a quote
         *      (?:[^'-]|-(?!-))*                       # non-string is a sequence of characters which aren't quotes or hyphens,
         */

        QRegExp rxSQL("^((?:(?:[^'-]|-(?!-))*|(?:'[^']*'))*)(--[^\\r\\n]*)([\\r\\n]*)(.*)$");	// set up regex to find end-of-line comment
        QString result;

        while(query.size() != 0)
        {
            int pos = rxSQL.indexIn(query);
            if(pos > -1)
            {
                result += rxSQL.cap(1) + rxSQL.cap(3);
                query = rxSQL.cap(4);
            } else {
                result += query;
                query = "";
            }
        }

        query = result.trimmed();
    }

    if (oldSize != query.size()) {
        // Remove multiple line breaks that might have been created by deleting comments till the end of the line but not including the line break
        query.replace(QRegExp("\\n+"), "\n");

        // Also remove any remaining whitespace at the end of each line
        query.replace(QRegExp("[ \t]+\n"), "\n");
    }
}

void SqlTableModel::buildQuery()
{
    setQuery(customQuery(true), true);
}
