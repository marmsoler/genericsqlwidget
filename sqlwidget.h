#ifndef SQLWIDGET_H
#define SQLWIDGET_H

#include <QWidget>
#include <QModelIndexList>

class SqlWidgetPrivate;
class SqlEditWidget;

class ExtendedTableView;
class QStackedWidget;
class QItemSelection;
class QAbstractItemDelegate;
class QUndoStack;
class QPushButton;

/*!
 * \brief The SqlWidget class
 * This class is indented to be used for all Sql stuff.
 * - Use setViewDelegate to set a specific delegate
 * -
 */
class SqlWidget: public QWidget
{
    Q_OBJECT
public:
    SqlWidget(QWidget* parent=nullptr);
    /*!
     * \brief loadDatabase
     * \param path
     * \param requiredColumns Columns which are required in the database.
     *        Sometime specific information is needed and so the columns must exist
     */
    void loadTable(QString database_path, QString table, QStringList requiredColumns = QStringList());
    /*!
     * \brief setViewDelegate
     * Use it to set a delegate to the tableView
     * \param delegate
     */
    void setViewDelegate(QAbstractItemDelegate* delegate);

    /*!
     * \brief addEditWidget
     * Use this function to add widget for editing specific values
     * See SqlColorEditWidget for example
     * TODO: be careful when adding index which is not in range!
     * \param widget
     * \param index
     */
    void addEditWidget(SqlEditWidget* widget, int index);

    /*!
     * \brief headerData
     * Returns the header data from the TableView
     * \param column
     * \return
     */
    QString headerData(int column) const;

    QString type(int column) const;

    ExtendedTableView *view() const;
    /*!
     * \brief changeEditWidget
     * Use this function to determine the widget which should be shown
     * \param itms
     * \return
     */
    virtual int changeEditWidget(QModelIndexList& itms) = 0;

    static QString QVariantTypeToSqlType(QVariant::Type type);

signals:
    /*!
     * \brief errorOccured
     * Connect to this signal if you want to get notified when an error occured
     * \param err Error message
     */
    void errorOccured(QString& err);
private:
    void updateEditWidget(const QItemSelection &selected, const QItemSelection &deselected);
    void clearEditWidget();
    void setValue(QVariant& value);
    void initConnection();
    void addRecord();
    void removeRecords();
    void addColumn();
    void removeColumns();
    void updateFilter(int column, const QString &value);

private:
    ExtendedTableView *m_view;
    QStackedWidget* m_edit;
    QPushButton* m_addRow;
    QPushButton* m_removeRow;
    QPushButton* m_addColumn;
    QPushButton* m_removeColumn;
    SqlWidgetPrivate* d{nullptr};
    QUndoStack* m_undoStack{nullptr};
};

#endif // SQLWIDGET_H
