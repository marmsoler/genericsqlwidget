#ifndef EDITWIDGET_H
#define EDITWIDGET_H

#include <QWidget>

/*!
 * \brief The SqlEditWidget class
 * Base class.
 */
class SqlEditWidget: public QWidget
{
    Q_OBJECT
public:
    SqlEditWidget(QWidget* parent = nullptr);

    void setValue(QVariant& value);

private:
    virtual void setWidgetValue(QVariant& value) = 0;

signals:
    /*!
     * \brief widgetValueChanged
     * Do not call this function!
     * \param value
     */
    void widgetValueChanged(QVariant& value);
public slots:
    void valueChanged(QVariant& value);
private:
    bool m_suppressValueChanges{false};
};

#endif // EDITWIDGET_H
