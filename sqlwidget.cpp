#include "sqlwidget.h"

#include "sqlwidgetprivate.h"
#include "sqleditwidget.h"
#include "sqleditdummywidget.h"

#include "extendedtableview.h"

#include "sqltablemodel.h"
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QSqlError>
#include <QSqlQuery>

#include <QHBoxLayout>
#include <QSortFilterProxyModel>
#include <QStackedWidget>
#include <QSplitter>
#include <QLabel>
#include <QAbstractItemModel>
#include "filtertableheader.h"
#include <QUndoStack>
#include <QShortcut>
#include <QPushButton>

#include "columnnamedialog.h"


#include "colordelegate.h"

SqlWidget::SqlWidget(QWidget *parent):
    QWidget(parent),
    d(new SqlWidgetPrivate())
{
    m_undoStack = new QUndoStack(this);

    QHBoxLayout* hBox = new QHBoxLayout();
    m_addRow = new QPushButton(tr("add Row"), this);
    m_removeRow = new QPushButton(tr("remove selected Row(s)"), this);
    m_addColumn = new QPushButton(tr("add Column"), this);
    m_removeColumn = new QPushButton(tr("remove selected Column(s)"), this);
    hBox->addWidget(m_addRow);
    hBox->addWidget(m_removeRow);
    hBox->addWidget(m_addColumn);
    hBox->addWidget(m_removeColumn);

    m_view = new ExtendedTableView(this);
    m_view->setModel(d->model);
    m_view->show();
    // do not edit the table directly, but use its own widget
    m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QVBoxLayout* vBox = new QVBoxLayout();
    vBox->addLayout(hBox);
    vBox->addWidget(m_view);
    QWidget* viewWidget = new QWidget(this);
    viewWidget->setLayout(vBox);

    m_edit = new QStackedWidget(this);

    // dummy widget
    // This shows information when no item is selected or over multiple columns
    SqlEditDummyWidget* dummy = new SqlEditDummyWidget(this);
    m_edit->addWidget(dummy);
    m_edit->setCurrentIndex(0);

    QSplitter* splitter = new QSplitter(this);
    splitter->addWidget(viewWidget);
    splitter->addWidget(m_edit);

    hBox = new QHBoxLayout();
    hBox->addWidget(splitter);
    setLayout(hBox);

    initConnection();
}

void SqlWidget::initConnection() {
    QShortcut* shortcut = new QShortcut(QKeySequence(tr("Ctrl+z")), this);
    connect(shortcut, &QShortcut::activated, this, [=] {m_undoStack->undo();});
    shortcut = new QShortcut(QKeySequence(tr("Ctrl+y")), this);
    connect(shortcut, &QShortcut::activated, this, [=] {m_undoStack->redo();});

    connect(m_view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &SqlWidget::updateEditWidget);

    connect(m_addRow, &QPushButton::clicked, this, &SqlWidget::addRecord);

    connect(m_removeRow, &QPushButton::clicked, this, &SqlWidget::removeRecords);

    connect(m_addColumn, &QPushButton::clicked, this, &SqlWidget::addColumn);

    connect(m_removeColumn, &QPushButton::clicked, this, &SqlWidget::removeColumns);

    // Set up filters
    connect(m_view->filterHeader(), &FilterTableHeader::filterChanged, this, &SqlWidget::updateFilter);
}

void SqlWidget::loadTable(QString database_path, QString table, QStringList requiredColumns) {
    d->db.close();

    d->db.setDatabaseName(database_path);
    if (!d->db.isValid())
        qDebug("Database not valid");

    d->table = table;

    // TODO: create new table when not existing

    assert(d->db.open());

    d->model->setTable(table);
    d->model->select(); // populate data

    m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    const bool show = true;

    if (!show)
        m_view->hideColumn(0); // don't show the ID
    qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->generateFilters(d->model->columnCount(), show);
}

void SqlWidget::setViewDelegate(QAbstractItemDelegate* delegate) {
    m_view->setItemDelegate(delegate);
}

void SqlWidget::addEditWidget(SqlEditWidget* widget, int index) {
    m_edit->insertWidget(index, widget);
    // otherwise the widget was appended and so the index does not match anymore
    assert(m_edit->indexOf(widget) == index);

    connect(widget, &SqlEditWidget::widgetValueChanged, this, &SqlWidget::setValue);
}

QString SqlWidget::headerData(int column) const {
    return d->model->headerData(column, Qt::Horizontal, Qt::DisplayRole).toString();
}

QString SqlWidget::type(int column) const {
    return QVariantTypeToSqlType(d->model->record().field(column).type());
}

ExtendedTableView* SqlWidget::view() const
{
    return m_view;
}

void SqlWidget::clearEditWidget() {
    m_edit->setCurrentIndex(0);
}

class AddRecordCommand: public QUndoCommand {
public:
    AddRecordCommand(SqlTableModel* model):
        m_model(model) {}

    void undo() override {
       for (int i=0; i < m_model->rowCount(); i++) {
           if (m_record.field(m_model->IDColumnIndex()).value().toInt() == m_model->getID(i)) {
               m_model->removeRow(i);
               break;
           }
       }
       m_model->submitAll();
       // otherwise an empty line stays in the model
       m_model->select();
    }

    void redo() override {
        if (m_row < 0) {
            // if the redo is called a second time, insert the row to a specific position
            // otherwise append it
            m_row = m_model->rowCount();
        }

        if (!m_model->insertRow(m_row))
            return;

        if (m_record.isEmpty()) {
            // Important: If a database field does not have a default value and the "not null" flag
            // is set, the data must be set manually here
            // the database does not have a default value for the name
            m_model->setDefaultName(m_row);
        } else {
            // redo was called a second time (redo - undo - redo)
            // restore the id
            m_model->setRecord(m_row, m_record);
        }

        // it is important too submit everything, because. Add column cannot be done by
        // insertColumn and must be done directly by a query. After the query a select() is
        // done and all unchanged modifications are lost
        m_model->submitAll();
        // with submitAll default values are set to the new record
        // but they are not yet in the model. So repopulate the model
        // to see the default values in the view
        m_model->select();

        m_record = m_model->record(m_row);


    }
private:
    int m_row{-1};
    SqlTableModel* m_model;
    QSqlRecord m_record;
};

class RemoveRecordCommand: public QUndoCommand {
public:
    typedef struct {
        int row;
        QSqlRecord record;
    } Record;

    RemoveRecordCommand(QTableView* view, SqlTableModel* model, QModelIndexList& idxList):
        m_view(view),
        m_model(model) {

        QList<int> rows;

        for (auto idx: idxList) {
            QSortFilterProxyModel* proxy = static_cast<QSortFilterProxyModel*>(m_view->model());
            int idxRow = proxy->mapToSource(idx).row();
            bool exists = false;
            for (int row: rows) {
                if (row == idxRow) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                rows.append(idxRow);
            }
        }

        // sorting is important, because when a smaller index is remove first,
        // the upper is anymore correct
        std::sort(rows.begin(), rows.end());

        Record r;
        for (auto row: rows) {
            r.row = row;
            r.record = m_model->record(row);
            m_records.append(r);
        }


    }

    void undo() override {
        for (int i = m_records.count() - 1; i >= 0; i--) {
            m_model->insertRow(m_records[i].row);
            m_model->setRecord(m_records[i].row, m_records[i].record);
        }
        m_model->submitAll();
        m_model->select();
    }

    void redo() override {

        for (int i = m_records.count() - 1; i >= 0; i--) {
            m_model->removeRow(m_records[i].row);
        }

        m_model->submitAll();
        m_model->select();
    }
private:
    QTableView* m_view;
    SqlTableModel* m_model;
    QList<Record> m_records;  
};

void SqlWidget::addRecord() {
    // TODO: do not execute again before the record was added correctly
    // How to check?
    m_undoStack->push(new AddRecordCommand(d->model));
}

void SqlWidget::removeRecords() {
    // QSortFilterProxyModel indexes
    QModelIndexList idxs = m_view->selectionModel()->selectedIndexes();
    m_undoStack->push(new RemoveRecordCommand(m_view, d->model, idxs));
}

QString SqlWidget::QVariantTypeToSqlType(QVariant::Type type) {
    if (type == QVariant::Type::String)
        return "TEXT";
    if (type == QVariant::Type::Double)
        return "REAL";
    if (type == QVariant::Type::Int)
        return "INTEGER";
    if (type == QVariant::Type::ByteArray)
        return "BLOB";
    // NUMERIC
    assert(false);
}

bool dropDatabaseColumns(QSqlDatabase* db, QString tableName, QStringList& deleteColumns) {
// https://stackoverflow.com/questions/5938048/delete-column-from-sqlite-table/5987838#5987838
// https://stackoverflow.com/questions/8442147/how-to-delete-or-add-column-in-sqlite

   QSqlQuery query("", *db);

   QSqlRecord record = db->record(tableName);

    QString columnNames;
    for (int i = 0; i < record.count(); i++) {
        QString column = record.fieldName(i);
        QString typeStr = SqlWidget::QVariantTypeToSqlType(record.field(i).type());
        bool found = false;
        for (auto c: deleteColumns) {
           if (c == column) {
               found = true;
               break;
           }
        }

        if (!found) {
            if (i > 0)
                columnNames += ",";
            columnNames += column + " " + typeStr;
        }
    }

    const QString tableBackup = "table_backup";

    QStringList queries;
    queries << QString("PRAGMA foreign_keys=OFF");
    queries << QString("BEGIN TRANSACTION");
    queries << QString("CREATE TABLE %1(%2);").arg(tableBackup).arg(columnNames);
    queries << QString("INSERT INTO %1 SELECT %2 FROM %3;").arg(tableBackup).arg(columnNames).arg(tableName);
    queries << QString("DROP TABLE %1").arg(tableName);
    queries << QString("ALTER TABLE %1 RENAME TO %2;").arg(tableBackup).arg(tableName);
    queries << QString("COMMIT");
    queries << QString("PRAGMA foreign_keys=ON");

    QString err;
    for (auto q: queries) {
        if (!query.exec(q)) {
            err = query.lastError().text();
        }
    }
    return true;
}

class AddColumnCommand: public QUndoCommand {
public:
    AddColumnCommand(QTableView* view, SqlTableModel* model, QSqlDatabase* db, QString tableName, QString columName, QString columnType):
    m_table(tableName),
    m_name(columName),
    m_type(columnType),
    m_db(db),
    m_model(model),
    m_view(view){}

    void undo() override {
        QStringList names(m_name);
        dropDatabaseColumns(m_db, m_table, names);

        qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->removeFilter();

//        m_model->selectStatement();
//        m_model->setTable(m_model->tableName());
//        m_model->selectStatement();
        m_model->update();
    }

    void redo() override {
        QString query_ = QString("ALTER TABLE %1 ADD COLUMN %2 %3").arg(m_table).arg(m_name).arg(m_type);
        QSqlQuery query("", *m_db);
        bool status = query.exec(query_);

        if (!status) {
            QString err = query.lastError().text();
            //emit errorOccured(err);
        } else {
            qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->addFilter();
        }

        m_model->update();
//        QString q = query.lastQuery();
//        query.finish();
//        q = query.lastQuery();
//        QSqlRecord r = query.record();
//        m_model->selectStatement();
//        m_model->setQuery(query);
//        QString e = m_model->lastError().text();
//        m_model->selectStatement();
//        m_model->setTable(m_model->tableName());
//        m_model->selectStatement();
    }
private:
    QString m_table;
    QString m_name;
    QString m_type;
    QSqlDatabase* m_db;
    SqlTableModel* m_model;
    QTableView* m_view;
};

void SqlWidget::addColumn() {
    // sql library needed, because qt does not support
    //d->model->insertColumn(m_view->model()->rowCount());

    // determine new unique Column name
    QSqlRecord record = d->db.record(d->table);
    QStringList columnNames;

    for (int i=0; i < record.count(); i++) {
        columnNames << record.fieldName(i);
    }
    ColumnNameDialog dialog(columnNames, this);
    if (dialog.exec() != QDialog::Accepted)
        return;

    QString type = "TEXT";
    m_undoStack->push(new AddColumnCommand(m_view, d->model, &d->db, d->table, dialog.columnName(), type));
}

class RemoveColumnsCommand: public QUndoCommand {
public:
    RemoveColumnsCommand(QTableView* view, SqlTableModel* model, QSqlDatabase* db, QString tableName, QStringList columnNames):
    m_table(tableName),
    m_columns(columnNames),
    m_db(db),
    m_model(model),
    m_view(view){}

    bool success() {
        return m_success;
    }

    QString error() {
        return m_err;
    }

    void undo() override {

        if (!m_success)
            return;

        // restore columns
        QString query_;
        QSqlQuery query("", *m_db);
        for (int i=0; i < m_columns.length(); i++) {

            query_ = QString("ALTER TABLE %1 ADD COLUMN %2 %3").arg(m_table).arg(m_columns[i]).arg(m_types[i]);

            bool status = query.exec(query_);

            if (!status) {
                m_err = query.lastError().text();
                //emit errorOccured(err);
            }
        }

        // restore entries
        QList<int>::const_iterator ids = m_ids.begin();
        for (QList<QList<QVariant>>::const_iterator row = m_entries.begin(); row != m_entries.end(); row++) {
            QString columns;

            QList<QVariant>::const_iterator column = row->begin();
            QStringList::const_iterator columnNames = m_columns.begin();
            QStringList::const_iterator type = m_types.begin();

            for (int c = 0; c < row->length(); c++) {
                if (c != 0)
                    columns += ", ";
                // how to update non string types?
                QString name = *columnNames;
                columns += name + " =";
                if (*type == "TEXT" || *type == "BLOB")
                    columns += "'" + column->toString() +"'";
                else if (*type == "INTEGER")
                    columns += QString::number(column->toInt());
                else if (*type == "REAL")
                    columns += column->toDouble();

                columnNames++;
                column++;
                type++;
            }
            query_ = QString("UPDATE %1 SET %2 WHERE ID = %3").arg(m_table).arg(columns).arg(*ids);

            if (!query.exec(query_))
                m_err = query.lastError().text();
            ids++;
        }

        for (int i=0; i < m_columns.count(); i++) {
            qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->addFilter();
        }

        m_model->update();
    }

    void redo() override {

        // TODO: store the data inside of the column to restore
        m_types.clear();
        m_entries.clear();
        m_err = "";
        m_success = true;
        // remove columns only if they are not mandatory

        QSqlRecord record = m_db->record(m_table);

        // if the column is required, it is not possible to delete it.
        // Abort and notify the user
        for (auto column: m_columns) {

            QSqlField field = record.field(column);
            assert(field.isValid());
            if (field.requiredStatus()) {
               m_success = false;
               m_err = QString("Column %1 is required and cannot be deleted").arg(column);
               m_types.clear();
               return;
            }

            m_types << SqlWidget::QVariantTypeToSqlType(field.type());
        }

        QSqlQuery query("", *m_db);
        if (!query.exec(QString("SELECT * FROM %1").arg(m_table))) {
            QString err = query.lastError().text();
            m_err = err;
            return;
        }


        // save entries from the deleting columns to undo
        while(query.next()) {

            QList<QVariant> columnValues;
            for (int i=0; i < m_columns.length(); i++)
                columnValues << query.value(m_columns[i]);
            m_entries << columnValues;

            // the ID column must have the name "ID"!
            m_ids << query.value("ID").toInt();
            query.isSelect();
        }

        dropDatabaseColumns(m_db, m_table, m_columns);

        for (int i=0; i < m_columns.count(); i++) {
            qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->removeFilter();
        }

        m_model->update();
    }
private:
    QString m_table;
    QStringList m_columns;
    QStringList m_types;
    QSqlDatabase* m_db;
    QString m_err{""};
    QList<QList<QVariant>> m_entries;
    QList<int> m_ids;
    SqlTableModel* m_model;
    QTableView* m_view;
    /*!
     * Indicates if redo was successful or not.
     * If not successful, m_err contains the error message
     */
    bool m_success{false};
};

void SqlWidget::removeColumns() {
    QModelIndexList list = m_view->selectionModel()->selectedIndexes();

    if (list.isEmpty())
        return;

    QSqlRecord record = d->db.record(d->table);
    QStringList columnNames;

    // check that column is only one time added, because
    // when multiple rows of a column are selected,
    // the column should be deleted only once ;)
    for (QModelIndex idx: list) {
        int idxColumn = idx.column();
        bool exists = false;
        QString columnName = record.fieldName(idxColumn);
        for (auto column: columnNames) {
            if (column == columnName) {
                exists = true;
                break;
            }
        }
        if (!exists)
            columnNames << columnName;
    }

    RemoveColumnsCommand* cmd = new RemoveColumnsCommand(m_view, d->model, &d->db, d->table, columnNames);
    m_undoStack->push(cmd); // calls also redo()
    if (!cmd->success()) {
        QString error = cmd->error();
        emit errorOccured(error);
        int index = m_undoStack->index();
        m_undoStack->setIndex(index - 1);
        assert(index - 1 == m_undoStack->index());
    }
}

void SqlWidget::updateFilter(int column, const QString& value) {

    // copied from sqlbrowser,
    // check for license issues
    d->model->updateFilter(column, value);
}

class SetValueCommand: public QUndoCommand {
public:
    SetValueCommand(QVariant newValue, QTableView* view, SqlTableModel* model, QModelIndexList& selectedIndexes):
        m_new(newValue),
        m_table(view),
        m_model(model) {
        QSortFilterProxyModel* proxy = static_cast<QSortFilterProxyModel*>(m_table->model());
        for (auto idx: selectedIndexes) {
            QModelIndex i = proxy->mapToSource(idx);
            assert(i.isValid());
            m_selectedIndexes.append(i);
            m_old.append(m_model->data(i));
        }
    }

    void undo() override {
       for (int i = 0; i < m_selectedIndexes.count(); i++) {
           m_model->setData(m_selectedIndexes[i], m_old[i]);
       }
       m_model->submitAll();
    }

    void redo() override {
        for (QModelIndex idx: m_selectedIndexes)
            m_model->setData(idx, m_new);
        m_model->submitAll();

    }
private:
    QList<QVariant> m_old;
    QVariant m_new;
    QTableView* m_table;
    SqlTableModel* m_model;
    QModelIndexList m_selectedIndexes;
};

void SqlWidget::setValue(QVariant& value) {
    QModelIndexList idxs = m_view->selectionModel()->selectedIndexes();
    m_undoStack->push(new SetValueCommand(value, m_view, d->model, idxs));
}

void SqlWidget::updateEditWidget(const QItemSelection &selected, const QItemSelection &deselected) {
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    QModelIndexList itms = m_view->selectionModel()->selectedIndexes();
    if (itms.isEmpty()) {
        clearEditWidget();
        return;
    }

    int column = itms[0].column();
    for (QModelIndex itm: itms) {
        if (itm.column() != column) {
           clearEditWidget();
           return;
        }
    }

    m_edit->setCurrentIndex(changeEditWidget(itms));
    QVariant data = itms.first().data(Qt::DisplayRole);
    static_cast<SqlEditWidget*>(m_edit->currentWidget())->setValue(data);

    m_edit->currentWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_edit->adjustSize();
}
